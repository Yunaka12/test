﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Coins : MonoBehaviour {


	GameObject gameController;
	GameObject  parent;
	GameObject grandparent;
	AudioSource audio;


	public AudioClip get;
	public float minHeight;
	public float maxHeight;

	private int coinget = 0;
	// Use this for initialization
	void Start () {
		parent = gameObject.transform.parent.gameObject;
		grandparent = parent.transform.parent.gameObject;
		gameController = GameObject.FindWithTag ("GameController");	
		audio = GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update () {
		if (coinget == 1) {
			parent.transform.Rotate (30, 20, 0);
			if (parent.transform.position.x <= -7) {
				coinget = 0;
				parent.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f); //rotateに値を代入
				parent.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

				float height = Random.Range (minHeight, maxHeight);
				Vector3 gp = parent.transform.position;
				gp.y = height;
				parent.transform.position = gp;

			}
			
		}
	}





	void OnTriggerEnter2D(Collider2D other)
	{
		if (coinget == 0) {
			audio.PlayOneShot (get, 1.0f);
			gameController.SendMessage ("IncreaseScore");
			coinget = 1;
			Invoke ("delay", audio.clip.length);


		}

	}


	void delay(){
		parent.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);

	}


}
